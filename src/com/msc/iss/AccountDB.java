package com.msc.iss;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class AccountDB {
	private static SessionFactory factory = null;

	public AccountDB(SessionFactory factory) {
		AccountDB.factory = factory;
	}

	/* Method to CREATE an account in the database */
	public Integer addAccount(Account account) {
		System.out.println(factory);
		Session session = factory.openSession();
		Transaction tx = null;
		Integer employeeID = null;
		try {
			tx = session.beginTransaction();
			employeeID = (Integer) session.save(account);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
		return employeeID;
	}

	public List<Account> listAccount(Criteria cr) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<Account> accounts = null;
		try {
			tx = session.beginTransaction();
			if (cr == null) {
				accounts = session.createQuery("FROM account").list();
			} else {
				accounts = cr.list();
			}
			accounts.stream().forEach((Account acct) -> {
				System.out.print("account name " + acct.getaccountname());
				System.out.print("  account number: " + acct.getAccountNum());
				System.out.println("  balance: " + acct.getbalance());
			});
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
		return accounts;
	}

	public void deleteAccount(Integer accountNumber) {
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Account employee = (Account) session.get(Account.class, accountNumber);
			session.delete(employee);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public Account updateAccount(Integer EmployeeID, Integer balance, String accountName) {
		Session session = factory.openSession();
		Account acc = null;
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			acc = (Account) session.get(Account.class, EmployeeID);
			if(acc!=null){
				if (accountName != null) {
					acc.setAccountname(accountName);
				}
				if (balance != null) {
					acc.setBalance(acc.getbalance() + balance);
				}
				session.update(acc);
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}

		return acc;
	}
}
