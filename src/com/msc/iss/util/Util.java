package com.msc.iss.util;

import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class Util {
	public void createDB() {
		ScriptRunner runner = null;
		System.out.println("creating");
		Connection conn = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/", "root", "");
			runner = new ScriptRunner(conn, false, true);
			runner.runScript(new InputStreamReader(getClass().getResourceAsStream("/import.sql")));

		} catch (Exception e) {
			e.printStackTrace();

		} finally {

			try {
				if (conn != null)
					conn.close();
				conn = null;
			} catch (SQLException e) {
				e.printStackTrace();

			}
		}
	}
}
