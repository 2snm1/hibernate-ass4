package com.msc.iss.util;

import java.util.Properties;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import com.msc.iss.Account;
import com.msc.iss.CreditAccount;

public enum HibernateUtil {
	INSTANCE;
	private HibernateUtil() {
	}

	// Property based configuration
	private static SessionFactory sessionJavaConfigFactory;

	private static SessionFactory buildSessionJavaConfigFactory() {
		try {
			Configuration configuration = new Configuration();

			// Create Properties, can be read from property files too
			Properties props = new Properties();

			props.put("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");
			props.put("hibernate.connection.url", "jdbc:mysql://localhost:3306/bank");
			props.put("hibernate.connection.username", "root");
			props.put("hibernate.connection.password", "");
			props.put("hibernate.c3p0.min_size", "5");
			props.put("hibernate.c3p0.max_size", "20");
			props.put("hibernate.c3p0.timeout", "1800");
//			props.put("hibernate.current_session_context_class", "thread");
			props.put("hibernate.c3p0.max_statements", "50");
			props.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
			props.put("hibernate.hbm2ddl.auto", "update");
			props.put("hibernate.hbm2ddl.import_files", "import.sql");
			props.put("hibernate.hbm2ddl.import_files_sql_extractor",
					"org.hibernate.tool.hbm2ddl.MultipleLinesSqlCommandExtractor");
			props.put("show_sql", true);
			configuration.setProperties(props);

			configuration.addAnnotatedClass(Account.class)/*.addAnnotatedClass(CreditAccount.class)*/;

			ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
					configuration.getProperties()).build();
			System.out.println("Hibernate Java Config serviceRegistry created");

			SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);

			return sessionFactory;
		} catch (Throwable ex) {
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionJavaConfigFactory() {
//		if (sessionJavaConfigFactory == null)
		return	sessionJavaConfigFactory = buildSessionJavaConfigFactory();
//		return sessionJavaConfigFactory;
	}

}