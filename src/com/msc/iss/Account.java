package com.msc.iss;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="account")
//@Inheritance(strategy=InheritanceType.JOINED)
public class Account  {


	// class variables
	@Column(name="account_name",nullable=false)
		String accountname;
		@Id
//		@GeneratedValue(generator=GenerationType.)
		@Column(name="account_number",nullable=false)
	    int accountNum;

		@Column(name="balance",nullable=false)
	    int balance;
//	    @OneToMany(fetch=FetchType.LAZY,mappedBy="accountNum")
//		private Set<CreditAccount> creditAccounts=null;
	    //default constructor for Account
	    Account()
	    {
	    	this.accountname="EMPTY";
	    	this.accountNum=0;
	    	this.balance=0;
	    }
	    
	    //overloaded constructor for Account
	    Account(String name, int num,int amt)
	    {
	            accountname=name;
	    	    accountNum=num;
	            balance=amt;
	    }
	    //make a deposit to the balance
	    public void deposit(int amt)
	    {
	    	    balance=balance+amt;
	    }
	    //make a withdrawal from the balance
	    public void withdraw(int amt)
	    {
	    	    balance=balance-amt;
	    }
	    //modifier to set the accountname
	    public void setaccountname(String name)
	    {
	    	    accountname = name;
	    }
	  //modifier to set the accountNumber
	    public void setaccountNum(int num)
	    {
	    	    accountNum = num;
	    }
	  //modifier to set the balance
	    public void setbalance(int num)
	    {
	    	    balance = num;
	    }
	  //accessor to get the accountname
	    public String getaccountname ( ) {
	    	 
	    	return accountname;
	    }
	    
	  //accessor to get the accountNumber
	    public int getaccountNum ( ) {
	   	 
	    	return accountNum;
	    }
	  //accessor to get the account balance
	    public int getbalance ( ) {
	      	 
	    	return balance;
	    }
	    
	    //print method 
	    public void print() {
	        System.out.println(accountname + " " + accountNum + " " + balance);
	      }

		public String getAccountname() {
			return accountname;
		}

		public void setAccountname(String accountname) {
			this.accountname = accountname;
		}

		public int getAccountNum() {
			return accountNum;
		}

		public void setAccountNum(int accountNum) {
			this.accountNum = accountNum;
		}

		public int getBalance() {
			return balance;
		}

		public void setBalance(int balance) {
			this.balance = balance;
		}

//		public Set<CreditAccount> getCreditAccounts() {
//			return creditAccounts;
//		}
//
//		public void setCreditAccounts(Set<CreditAccount> creditAccounts) {
//			this.creditAccounts = creditAccounts;
//		}
	}
