package com.msc.iss;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.TitledBorder;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Order;

import com.msc.iss.util.HibernateUtil;
import com.msc.iss.util.ScriptRunner;
import com.msc.iss.util.Util;

public class JavaBank extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static SessionFactory factory = null;
	// Make these variables publicly available
	public String Name;
	public int Accountnum;
	public int Balance;
	
	private List<Account> myAccounts = null;
	private Criteria cr = null;
	// JPanel for user inputs
	private JPanel inputDetailJPanel;

	// JLabel and JTextField for account name
	private JLabel NameJLabel;
	private JTextField NameJTextField;

	// JLabel and JTextField for account number
	private JLabel AccountnumJLabel;
	private JTextField AccountnumJTextField;

	// JLabel and JTextField for balance
	private JLabel BalanceJLabel;
	private JTextField BalanceJTextField;

	// JLabel and JTextField for withdraw
	private JLabel DepositJLabel;
	private JTextField DepositJTextField;

	// JLabel and JTextField for Withdraw
	private JLabel WithdrawJLabel;
	private JTextField WithdrawJTextField;

	// JButton to create account
	private JButton CreateAccountJButton;

	// JButton to delete account
	private JButton DeleteAccountJButton;

	// JButton to make transaction
	private JButton performTransactionJButton;
	private JButton TransactionJButton;

	// JButton to display account
	private JButton DisplayJButton;

	// JLabel and JTextArea to display account details
	private JLabel displayJLabel;
	private static JTextArea displayJTextArea;
	private AccountDB adb = null;
	// constants
	// public final static Maximum Accounts that can be created;
	public final static int MaxAccounts = 10;

	// two-dimensional array to store Account details

	static final String path = "C:\\Log\\account.ser";
private Util util=new Util();
	// constructor

	public JavaBank() {
		try {
			System.out.println(factory);
			util.createDB();
			factory = HibernateUtil.getSessionJavaConfigFactory();
			adb = new AccountDB(factory);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		createUserInterface();
	}

	public boolean isValidNumber(String number) {
		if (number != null) {
			Pattern pattern = Pattern.compile("\\d+");
			Matcher match = pattern.matcher(number);

			return match.matches();
		} else {
			return false;
		}
	}

	// create and position GUI components; register event handlers
	private void createUserInterface() {
		// get content pane for attaching GUI components
		Container contentPane = getContentPane();

		// enable explicit positioning of GUI components
		contentPane.setLayout(null);

		// set up inputDetailJPanel
		inputDetailJPanel = new JPanel();
		inputDetailJPanel.setBounds(16, 16, 208, 250);
		inputDetailJPanel.setBorder(new TitledBorder("Input Details"));
		inputDetailJPanel.setLayout(null);
		contentPane.add(inputDetailJPanel);

		// set up NameJLabel
		NameJLabel = new JLabel();
		NameJLabel.setBounds(8, 32, 90, 23);
		NameJLabel.setText("Name:");
		inputDetailJPanel.add(NameJLabel);

		// set up NameJTextField
		NameJTextField = new JTextField();
		NameJTextField.setBounds(112, 32, 80, 21);
		NameJTextField.setHorizontalAlignment(JTextField.RIGHT);
		inputDetailJPanel.add(NameJTextField);

		// set up AccountnumJLabel
		AccountnumJLabel = new JLabel();
		AccountnumJLabel.setBounds(8, 56, 100, 23);
		AccountnumJLabel.setText("Account Number:");
		inputDetailJPanel.add(AccountnumJLabel);

		// set up AccountnumTextField
		AccountnumJTextField = new JTextField();
		AccountnumJTextField.setBounds(112, 56, 80, 21);
		AccountnumJTextField.setHorizontalAlignment(JTextField.RIGHT);
		inputDetailJPanel.add(AccountnumJTextField);

		// set up BalanceJLabel
		BalanceJLabel = new JLabel();
		BalanceJLabel.setBounds(8, 80, 60, 23);
		BalanceJLabel.setText("Balance:");
		inputDetailJPanel.add(BalanceJLabel);

		// set up BalanceTextField
		BalanceJTextField = new JTextField();
		BalanceJTextField.setBounds(112, 80, 80, 21);
		BalanceJTextField.setHorizontalAlignment(JTextField.RIGHT);
		inputDetailJPanel.add(BalanceJTextField);

		// set up DepositJLabel
		DepositJLabel = new JLabel();
		DepositJLabel.setBounds(8, 104, 80, 23);
		DepositJLabel.setText("Deposit:");
		inputDetailJPanel.add(DepositJLabel);

		// set up DepositJTextField
		DepositJTextField = new JTextField();
		DepositJTextField.setBounds(112, 104, 80, 21);
		DepositJTextField.setHorizontalAlignment(JTextField.RIGHT);
		inputDetailJPanel.add(DepositJTextField);

		// set up WithdrawJLabel
		WithdrawJLabel = new JLabel();
		WithdrawJLabel.setBounds(8, 128, 60, 23);
		WithdrawJLabel.setText("Withdraw:");
		inputDetailJPanel.add(WithdrawJLabel);

		// set up WithdrawJTextField
		WithdrawJTextField = new JTextField();
		WithdrawJTextField.setBounds(112, 128, 80, 21);
		WithdrawJTextField.setHorizontalAlignment(JTextField.RIGHT);
		inputDetailJPanel.add(WithdrawJTextField);

		// so that the user has to click relevant buttons for the items to be
		// visible
		DepositJTextField.setEditable(false);
		BalanceJTextField.setEditable(false);
		WithdrawJTextField.setEditable(false);
		// set up CreateAccountButton
		CreateAccountJButton = new JButton();
		CreateAccountJButton.setBounds(112, 152, 80, 24);
		CreateAccountJButton.setText("Create");
		inputDetailJPanel.add(CreateAccountJButton);
		CreateAccountJButton.addActionListener(

		new ActionListener() {
			// event handler called when CreateAccountJButton
			// is clicked
			public void actionPerformed(ActionEvent event) {
				CreateAccountJButtonActionPerformed(event);
			}

		}

		); // end call to addActionListener

		// set up DeleteAccountButton
		DeleteAccountJButton = new JButton();
		DeleteAccountJButton.setBounds(16, 152, 80, 24);
		DeleteAccountJButton.setText("Delete");
		inputDetailJPanel.add(DeleteAccountJButton);
		DeleteAccountJButton.addActionListener(

		new ActionListener() // anonymous inner class
				{
					// event handler called when DeleteAccountJButton
					// is clicked
					public void actionPerformed(ActionEvent event) {
						DeleteAccountJButtonActionPerformed(event);

					}

				}

				); // end call to addActionListener

		// set up TransactionJButton
		TransactionJButton = new JButton();
		TransactionJButton.setBounds(16, 180, 176, 24);
		TransactionJButton.setText("Make Transaction");
		inputDetailJPanel.add(TransactionJButton);
		TransactionJButton.addActionListener(

		new ActionListener() // anonymous inner class
				{
					// event handler called when TransactionJButton
					// is clicked
					public void actionPerformed(ActionEvent event) {
						inputDetailJPanel.add(performTransactionJButton);
						inputDetailJPanel.remove(TransactionJButton);
						;
						TransactionJButtonActionPerformed(event);
					}

				} // end anonymous inner class

				); // end call to addActionListener

		// set up TransactionJButton
		performTransactionJButton = new JButton();
		performTransactionJButton.setBounds(16, 180, 176, 24);
		performTransactionJButton.setText("Perform the Transaction");
		performTransactionJButton.addActionListener(

		new ActionListener() // anonymous inner class
				{
					// event handler called when TransactionJButton
					// is clicked
					public void actionPerformed(ActionEvent event) {
						performTransactionJButtonActionPerformed(event);

						inputDetailJPanel.add(TransactionJButton);

						inputDetailJPanel.remove(performTransactionJButton);
					}

				} // end anonymous inner class

				); // end call to addActionListener

		// set up DisplayJButton
		DisplayJButton = new JButton();
		DisplayJButton.setBounds(16, 208, 176, 24);
		DisplayJButton.setText("Display Accounts");
		inputDetailJPanel.add(DisplayJButton);
		DisplayJButton.addActionListener(

		new ActionListener() // anonymous inner class
				{
					// event handler called when TransactionJButton
					// is clicked
					public void actionPerformed(ActionEvent event) {
						DisplayJButtonActionPerformed(event);
					}

				} // end anonymous inner class

				); // end call to addActionListener

		// set up displayJLabel
		displayJLabel = new JLabel();
		displayJLabel.setBounds(240, 16, 150, 23);
		displayJLabel.setText("Account Details:");
		contentPane.add(displayJLabel);

		// set up displayJTextArea
		displayJTextArea = new JTextArea();
		JScrollPane scrollPane = new JScrollPane(displayJTextArea);
		scrollPane.setBounds(240, 48, 402, 184);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		contentPane.add(scrollPane);

		display();

		// clear other JTextFields for new data
		NameJTextField.setText(" ");
		AccountnumJTextField.setText("0");
		BalanceJTextField.setText("0");
		DepositJTextField.setText("0");
		WithdrawJTextField.setText("0");

		// set properties of application's window
		setTitle("Java Bank"); // set title bar string
		setSize(670, 308); // set window size
		setVisible(true); // display window

	} // end method createUserInterface

	private void CreateAccountJButtonActionPerformed(ActionEvent event) {
		// System.out.println("Create Account Button Clicked");
		String newString = AccountnumJTextField.getText().trim();
		displayJTextArea.setText("");

		Name = "";
		// Get Name from Text Field
		Name = NameJTextField.getText();

		// Get Accountnum from Text Field and convert to int unless blank then
		// set to 0
		if (AccountnumJTextField.getText() == "0") {
			Accountnum = 0;
		} else {
			System.out.println(AccountnumJTextField.getText());
			if (!isValidNumber(AccountnumJTextField.getText())) {
				newString = JOptionPane.showInputDialog(null, "Invalid account number");
			}
			if (!isValidNumber(newString)) {
				displayJTextArea.setText("invalid input please try again");

			} else {
				Accountnum = Integer.parseInt(newString);
				// Get Balance from Text Field and convert to int unless blank
				// then set
				// to 0
				if (BalanceJTextField.getText() == "0") {
					Balance = 0;
				} else {
					Balance = Integer.parseInt(BalanceJTextField.getText());
				}

				// int emptyAccount = 11;
				displayJTextArea.setText("Account Name\t " + "Account Number\t " + "Account Balance\t\n");
Integer res=null;
				if (((Name != "")&&Name!=null) && (Accountnum != 0)) {
					Account acc = new Account(Name, Accountnum, Balance);
					res=adb.addAccount(acc);
System.out.println(res);
					if(res!=null){
						displayJTextArea.append(acc.getaccountname() + "\t " + acc.getAccountNum() + "\t\t "
								+ acc.getbalance());
					}else{
						displayJTextArea.setText("there is a user with that account number");
					}
				} else {
					displayJTextArea.setText("Both the Name field and Account Number must be completed");
				}

			}
		}

		// clear other JTextFields for new data
		NameJTextField.setText(" ");
		AccountnumJTextField.setText("0");
		BalanceJTextField.setText("0");
		DepositJTextField.setText("0");
		WithdrawJTextField.setText("0");

	}

	private void DeleteAccountJButtonActionPerformed(ActionEvent event) {
		System.out.println("deleting");

		adb.deleteAccount(Integer.parseInt(AccountnumJTextField.getText()));
		display();
		NameJTextField.setText(" ");
		AccountnumJTextField.setText("0");
		BalanceJTextField.setText("0");
		DepositJTextField.setText("0");
		WithdrawJTextField.setText("0");

	}

	private void TransactionJButtonActionPerformed(ActionEvent event) {
		AccountnumJTextField.setText("0");
		BalanceJTextField.setText("0");
		DepositJTextField.setText("0");
		WithdrawJTextField.setText("0");
		DepositJTextField.setEditable(true);
		WithdrawJTextField.setEditable(true);

		NameJTextField.setEditable(false);

	}

	private void performTransactionJButtonActionPerformed(ActionEvent event) {

		displayJTextArea.setText("");

		if (myAccounts.isEmpty()) {
			displayJTextArea.setText("No Accounts currently created");

		} else {
			if (!isValidNumber(AccountnumJTextField.getText()) || !isValidNumber(DepositJTextField.getText())) {
				displayJTextArea.setText("Invalid input please try again");

			} else {

				if (!isValidNumber(DepositJTextField.getText()) || !isValidNumber(WithdrawJTextField.getText())) {
					displayJTextArea.setText("invalid input please try again");
				} else {
					// get user input
					int Deposit = Integer.parseInt(DepositJTextField.getText());
					int Withdraw = Integer.parseInt(WithdrawJTextField.getText());

					Account acc = adb.updateAccount(Integer.parseInt(AccountnumJTextField.getText()), Deposit
							- Withdraw, null);
					if (acc != null) {
						displayJTextArea.setText("Account Name\t " + "Account Number\t " + "Account Balance\t\n");

						displayJTextArea.append(acc.getaccountname() + "\t " + acc.getAccountNum() + "\t "
								+ acc.getbalance());
					} else {
						displayJTextArea.setText("There is no account with that number");

					}

					// clear other JTextFields for new data
					AccountnumJTextField.setText("0");
					BalanceJTextField.setText("0");
					DepositJTextField.setText("0");
					WithdrawJTextField.setText("0");
				}

			}

		}
		DepositJTextField.setEditable(false);
		WithdrawJTextField.setEditable(false);

		NameJTextField.setEditable(true);

	}

	private void DisplayJButtonActionPerformed(ActionEvent event) {

		Name = NameJTextField.getText();
		displayJTextArea.setText("");
		display();
		// clear other JTextFields for new data
		NameJTextField.setText(" ");
		NameJTextField.setEditable(true);
		AccountnumJTextField.setText("0");
		BalanceJTextField.setText("0");
		DepositJTextField.setText("0");
		WithdrawJTextField.setText("0");

	}

	private void display() {
		cr = factory.openSession().createCriteria(Account.class);
		cr.addOrder(Order.asc("accountNum"));
		cr.setFirstResult(0);
		myAccounts = adb.listAccount(cr);
		if (myAccounts == null || myAccounts.isEmpty()) {
			displayJTextArea.setText("Welcome to Java Bank - There are currently no Accounts created");
		} else {
			displayJTextArea.setText("Account Name\t " + "Account Number\t " + "Account Balance\t\n");

			myAccounts
					.stream()
					.filter(p -> p != null)
					.forEach(
							(Account acc) -> {
								displayJTextArea.append(acc.getaccountname() + "\t " + acc.getAccountNum() + "\t\t "
										+ acc.getbalance() + "\n");
							});
		}

	}

	public static void main(String[] args) {
		// Populate arrays with the word EMPTY
		// so we can check to see if the values are empty later

		JavaBank application = new JavaBank();
		// application.dispatchEvent(e);
		application.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		application.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				JFrame frame = (JFrame) e.getSource();

				int result = JOptionPane.showConfirmDialog(frame, "Are you sure you want to exit the application?",
						"Exit Application,", JOptionPane.YES_NO_OPTION);

				if (result == JOptionPane.YES_OPTION)
					frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			}
		});
	}

}
