package com.msc.iss;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "credit_account")
// @PrimaryKeyJoinColumn(name="account_number")
public class CreditAccount extends Account {

	private Account acc=null;
	public void setAcc(Account acc) {
		this.acc = acc;
	}

	public void setCreditLimit(int creditLimit) {
		this.creditLimit = creditLimit;
	}

	public void setCreditAccountNumber(int creditAccountNumber) {
		this.creditAccountNumber = creditAccountNumber;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Account getAcc() {
		return acc;
	}

	@Column(name = "credit_limit")
	int creditLimit;
	@Id
	@Column(name = "credit_account_number")
	int creditAccountNumber;
	@Column(name = "account_number")
	int accountNumber;

	// default constructor for CreditAccount
	CreditAccount() {
		super();
		this.creditLimit = 100;
	}

	// overloaded constructor for CreditAccount
	CreditAccount(String name, int num, int amt, int credit) {
		super(name, num, amt);
		this.creditLimit = credit;

	}

	// modifier to set the account creditlimit
	public void setcreditlimit(int num) {
		creditLimit = num;
	}

	// accessor to get the account creditlimit
	public int getcreditlimit() {

		return creditLimit;
	}

	// print method
	public void print() {
		System.out.println(accountname + " " + accountNum + " " + balance + " " + creditLimit);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!this.getClass().equals(obj.getClass()))
			return false;
		CreditAccount obj2 = (CreditAccount) obj;
		if ((this.creditAccountNumber == obj2.getCreditAccountNumber())
				&& (((Integer) this.creditLimit).equals(obj2.getCreditAccountNumber()))) {
			return true;
		}
		return false;

	}

	public int getCreditLimit() {
		return creditLimit;
	}

	public int getCreditAccountNumber() {
		return creditAccountNumber;
	}

	public int getAccountNumber() {
		return accountNumber;
	}

	@Override
	public int hashCode() {
		int tmp = 0;
		tmp = ((Integer) (creditAccountNumber + creditLimit)).hashCode();
		return tmp;
	}
}
