SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `bank` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `bank` ;

-- -----------------------------------------------------
-- Table `bank`.`account`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bank`.`account` (
  `account_number` INT NOT NULL,
  `account_name` VARCHAR(45) NULL,
  `balance` VARCHAR(45) NULL,
  PRIMARY KEY (`account_number`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bank`.`credit_account`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bank`.`credit_account` (
  `credit_limit` INT NOT NULL,
  `account_number` INT NOT NULL,
  `credit_account_number` INT NOT NULL,
  PRIMARY KEY (`credit_account_number`),
  INDEX `fk_credit_account_account_idx` (`account_number` ASC),
  CONSTRAINT `fk_credit_account_account`
    FOREIGN KEY (`account_number`)
    REFERENCES `bank`.`account` (`account_number`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
